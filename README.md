SpiroFS
=======

SpiroFS is a fileserver backend for Salt focusing on deployment and saltenv
management. It allows automated systems to push deployment bundles to Salt
without giving them undue permissions. It is dynamic and does not require
on-going reconfiguration as your technology stack grows and changes.

Full documentation is https://saltstack.com/spirofs

Installation
------------
0. Have pip for the Python that salt is installed into
1. `pip install spirofs` (Adapt as necessary)
2. Configure the salt master (see [docs](https://spirostack.com/spirofs/#configuration))
3. Restart the salt master

Note: libsodium is used, but is pre-packaged in the binary wheels. If these are
not used on your system for whatever reason, install either `libsodium-dev` (apt)
or `libsodium-devel` (yum).
